import memoizeOne from "memoize-one";
import { addSeconds, isAfter } from "date-fns/fp";

// Dummy fetch that depends on time, just like a record in a db:
// const fetchRecord = argObj => ({
//   id: argObj.id,
//   data: new Date().getSeconds()
// });
const fetchRecord = (...args) => console.log(args);

const withTimestamp = fn => (...args) => fn(new Date(), ...args);

const stampedFetchRecord = withTimestamp(fetchRecord);

const isPrevCallOld = ([now, ...args], [prevTime, ...prevArgs]) =>
  isAfter(addSeconds(30, prevTime), now);

// Memoized version that should get a new key/id for the value at a certain rate
export const cachedFetchRecord = memoizeOne(stampedFetchRecord, isPrevCallOld);

export const recordFetcherCached = argObj => cachedFetchRecord;

const fetchRecordCached = args => {};
