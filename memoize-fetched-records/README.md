# Using memoize to cache fetched records

some simple way to cache records that are fetched with some function that sends a specific request, but if enough time has passed we don't want to get the cached record but rather a fresh new one.

As the functional mind I try to evolve into, I thought if memoization could help solve this. (Us familiar with the new react hooks might have used useCallback/useMemo that both are memoizers).

`memoize-one` would be a good candidate here to prevent a growing cache:
