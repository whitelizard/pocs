# FRONTEND DEVELOPMENT WITH REACT - A GETTING STARTED GUIDE

# Tech stack (language & framework)

## What about TypeScript?

Skip this and just go on below to get introduced to React FE dev. But if the question "Why not TypeScript?" echoes in your head, perhaps get some answers here: [My summary of JavaScript vs TypeScript](https://gitlab.com/whitelizard/pocs/-/blob/master/fe-dev/ts.md)

## Why React?

- [State of JS](https://2019.stateofjs.com)
- [Top Javascript frameworks/topics 2020](https://medium.com/javascript-scene/top-javascript-frameworks-and-topics-to-learn-in-2020-and-the-new-decade-ced6e9d812f9)
- [Top JavaScript Frameworks and Tech Trends for 2021](https://medium.com/javascript-scene/top-javascript-frameworks-and-tech-trends-for-2021-d8cb0f7bda69)

# React Intro

### Be sure to skip everything regarding `class` based React components. Instead focus on learning **React Hooks** (functional react components).

- [The missing introduction to React](https://medium.com/javascript-scene/the-missing-introduction-to-react-62837cb2fd76)
- [Thinking in React Hooks](https://wattenberger.com/blog/react-hooks)
- [Official docs](https://reactjs.org/docs/hooks-intro.html)

## Example App

Browse a simple example app that uses some basic Material-UI setup and components. Includes mocked async fetch and custom hook: [Basic React MUI app](https://codesandbox.io/s/react-example-1a-6dut0?file=/src/App.js)

## Intermediate React-JavaScript challenge (Skip if you're new to React & React Hooks)

Solve the React/JS Puzzle: [React/JS Challenge](https://codepen.io/whitelizard/pen/gOpWKOv) (solution at the bottom of this file)

## Getting started

### create-react-app

As they [recommend in the official react docs](https://reactjs.org/docs/create-a-new-react-app.html), use [create-react-app](https://create-react-app.dev/) to start a new React SPA (single page application) project with a great production ready toolchain.

### ESLint + Prettier

Make sure to use ESLint & Prettier for your project and your editor. It will save you and your teammates so much time not having to think about code style at all, and that the style always will be 100% consistent. Install both ESLint & Prettier as extensions/plugins for your favorite dev environment - search the web for a guide for your editor.

Most common standard base config: [airbnb](https://www.npmjs.com/package/eslint-config-airbnb)

__Suggested scripts in `package.json`:__
```json
  "scripts": {
    "format": "prettier src --write",
    "lint": "eslint src",
    ...
  }
```

With the above scripts you can run `npm run format` and `npm run lint`.

To make our format script work, we need to `npm i -D prettier` and create a config file.

__Suggested `.prettierrc.json`:__
```json
{​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
  "endOfLine": "lf",
  "printWidth": 100,
  "singleQuote": true,
  "trailingComma": "all",
  "useTabs": false,
  "tabWidth": 2
}​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
```

If you have used some bolierplate to initialize your project, like `create-react-app`, it might be the case that `eslint` is already among your dependencies, and installing it explicitly could cause version collision.

Otherwise, you install it simply by: `npm i -D eslint`

This example eslint config is of a more opinionated nature than the snippets above. It is however better to start with a more strict set of linting rules, and remove or modify as a project or team matures, than to try to add more linter rules later.
__Suggested `.eslintrc.json`:__
```json
{
  "parser": "babel-eslint",
  "extends": [
    "react-app",
    "react-app/jest",
    "airbnb",
    "plugin:react/jsx-runtime",
    "plugin:fp/recommended",
    "plugin:functional/external-recommended",
    "plugin:functional/lite",
    "plugin:promise/recommended",
    "plugin:prettier/recommended",
    "prettier"
  ],
  "plugins": ["prettier", "no-null", "react", "functional", "fp", "prefer-arrow", "promise"],
  "env": {
    "browser": true,
    "es6": true
  },
  "rules": {
    "prettier/prettier": "error",
    "no-null/no-null": 2,
    "no-mixed-operators": [0],
    "import/prefer-default-export": "off",
    "import/no-default-export": ["error"],
    "no-underscore-dangle": ["error", { "allow": ["__"] }],
    "no-return-assign": ["error", "except-parens"],
    "no-nested-ternary": [0],
    "no-void": [0],
    "react/jsx-filename-extension": [0],
    "react/jsx-props-no-spreading": [0],
    "react/require-default-props": [0],
    "react/prop-types": [0],
    "max-lines": ["warn", 300],
    "max-lines-per-function": ["warn", 200],
    "complexity": ["warn", 10],
    "max-nested-callbacks": ["warn", 3],
    "max-depth": ["warn", 4],
    "max-params": ["warn", 3],
    "fp/no-nil": [0],
    "fp/no-unused-expression": [0],
    "fp/no-rest-parameters": [0],
    "fp/no-let": [0],
    "fp/no-mutation": [
      "warn",
      {
        "exceptions": [
          { "object": "window" },
          { "object": "document" },
          { "object": "error" },
          { "property": "current" },
          { "property": "propTypes" }
        ]
      }
    ],
    "fp/no-mutating-methods": [
      "warn",
      {
        "allowedObjects": ["R", "history"]
      }
    ],
    "functional/immutable-data": [0],
    "functional/functional-parameters": ["warn"],
    "functional/no-let": ["warn"],
    "promise/always-return": [0],
    "promise/catch-or-return": [0],
    "promise/param-names": [0],
    "promise/no-return-wrap": ["error", { "allowReject": true }],
    "prefer-arrow/prefer-arrow-functions": [
      "error",
      {
        "disallowPrototype": true,
        "singleReturnOnly": false,
        "classPropertiesAllowed": false
      }
    ],
    "prefer-arrow-callback": ["error", { "allowNamedFunctions": true }],
    "func-style": ["error", "expression"]
  }
}
```

To make the above config work in your project, we need all base configs and plugins installed:

`npm i -D eslint-config-airbnb eslint-config-prettier eslint-plugin-no-null eslint-plugin-fp eslint-plugin-functional eslint-plugin-import eslint-plugin-prettier eslint-plugin-promise eslint-plugin-prefer-arrow`

## Husky

Use [Husky](https://www.npmjs.com/package/husky) to get configurable pre-commit hooks (and other possibilities) for your code base. This is a good habit for code quality -- a fantastic pain relief for collaborating developers, ensuring code consistency.

__Example config in `package.json`:__
```json
  "husky": {
    "hooks": {
      "pre-commit": "npm run format && npm run lint"
    }
  }
```

## Editorconfig

There's a great base editor configuration system that is useful to enable in every editor or IDE, called **editorconfig**. It complements Prettier with some more generic settings and formatting. You can probably find it as a plugin for your editor (search for "EditorConfig") and then add a file `.editorconfig` to you project root:

```
root = true

[*]
indent_style = space
indent_size = 2
end_of_line = lf
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
```

# JS FE Guidelines

## Airbnb JavaScript Style Guide

Great read:
https://github.com/airbnb/javascript

## Recommendations

These are some collected recommendations for modern, safe, coherent JS development. Most **code style guidelines should be rules** in the ESLint config, but not all good principles can be made into linting rules. Here are some rules of thumb on top of a comprehensive lint rule-set (with some overlap):

- Prefer **pure functions** over any other operational construct.
- Never use `null`.
- **Never use loops**, always use declarative transformation functions like `map`, `filter`, `reduce` etc (native JS, but preferrably ramda).
- Never use `class` or `this` (and only use `new` for built-in or imported constructors, e.g. `Date`).
  - Use **factory functions** instead of classes, which just means a function that returns an object (or possibly some other construct like array or function).
  - With **React**, use only functional components, using **React Hooks**.
- Use **immutable** operations over mutable.
  - Using e.g. ramda instead of native JS for all array & object operations (`map`, `filter`, `sort`, `reverse`) ensures that the operations are immutable.
- Never use `function`, always `=>`.
- Use **destructuring** as preferred way to "unpack" data structures, set defaults & (re)name values, also for deep unpacking through inner destructuring. Ex:
  - `const {​​​​​​​ name: first = '', address: {​​​​​​​ street, city }​​​​​​​​​​​​​​​​​​​​​ = {​​​​​​​​​​​​​​​​​​​​​}​​​​​​​​​​​​​​​​​​​​​ }​​​​​​​​​​​​​​​​​​​​​ = user;`
  - Be sure to use "no-null" eslint rule (included in example eslint config above) because defaults will not work on null values.
- Use spread operator for shallow copy, omit & rest.
  - `[...list, item] // append item, immutably (shallow copy)`
  - `const {​​​​​​​​​​​​​​​​​​​​​ id: omittedId, ...pureUser }​​​​​​​​​​​​​​​​​​​​​ = user; // omit & rest`
- Use `forEach` **only** for side-effects.
- Don't use any of the "broken" or problematic keywords in JS, like `with`, `instanceof`, `delete`.
  - Even `typeof` is problematic if you're not familiar with exactly how it works on all different data types in JS.
    - Use checkers from some util lib instead, preferably [lodash](https://lodash.com/).
- Use a functional utility library for **ALL date/time operations** (e.g. [date-fns](https://date-fns.org/), avoid heavy class based libs like moment).
- Use either `async`/`await` or `.then()`, depending on which pattern simplifies the code in each particular case.
- **Self-document code** by creating local variables and functions with descriptive naming.
- Functional composition is powerful! [ramda](https://ramdajs.com/) or equivalent.

- Avoid using **inline** style/CSS on components/elements in JSX/HTML
  - Use styling classes instead, via `className` in React
  - Combine classes by using the lib `clsx`
  - Use inline `style` only for highly dynamic values used e.g. for animation

### Notes on naming

- **camelCase** for naming **JSON** fields, **JS** variables and methods.
  - Which then also implies camelCase for fields in JSON **network protocols**.
- **PascalCase** for naming **React** functional components.
- **PascalCase** for React component **filenames**.
- **kebab-case** for other **JS files and folders** (being collection files, libraries, etc).
  - **camelCase** is also fine, and getting more common.
- **kebab-case** for naming **API** endpoints, **URLs**.
[REST resource naming](https://restfulapi.net/resource-naming/)

# State management

## Redux

Pros:
- Flux architecture (unidirectional flow)
- Lots of tools, middleware & extensions
- Small, simple & functional

Cons:
- No state model, only data

Possible simpler React equivalent: `createContext` + `useContext` + `useReducer`.

Redux is probably the most widely known global state manager for Javascript. It was the product of facebook's Flux architecture and has helped in popularizing **event based state management, reducers and immutability**. The main workings of redux can actually be implemented in under 10 lines of code (shown in the last chapter of my article https://realworldjs.medium.com/a-javascript-pattern-progression-from-oop-to-functional-style-fp-79b5717e9345).

Redux has gotten a large ecosystem with extensions, tools, middleware etc, and with this also some dislike -- people that find that the boilerplates of modern redux apps are too complex and point out redux as the cause. But this is a misunderstanding. Redux is a small and simple module. It is also not a frontend library, but can be used with great benefit in backend (NodeJS).

For simpler React apps or small specific pieces of global state, `createContext`, `useContext` & `useReducer` is a great alternative. 

There has also lately been raised concerns about that Redux applications often get complex and error prone due to that one might miss to **model** the application correctly through its flow and different **states**. An understanding around this can be retrieved in a 2-part article "Redux is half of a pattern" beginning with: https://dev.to/davidkpiano/redux-is-half-of-a-pattern-1-2-1hd7.

The author of that article is also the author of a rapidly growing library and ecosystem, **XState**, which is an implementation of **finite state machines** for Javascript.

## Recommendation

I'm not saying "always use XState" (I kind of do actually), but it is a great state manager that can help to avoid many pitfalls. I am suggesting to at least use some event based state management (for anything more complex than a very simple app). Perhaps simply React context + useReducer, perhaps Redux, but there is a good number of other event based state libraries out there. I am also suggesting to be wary about an application's or component's different possible states, and to **model** them in a **safe** manner instead of the naive way (https://kentcdodds.com/blog/stop-using-isloading-booleans, https://dev.to/mpocock1/usestate-vs-usereducer-vs-xstate-part-1-modals-569e).

## XState

- https://xstate.js.org
- [Redux is half a pattern](https://dev.to/davidkpiano/redux-is-half-of-a-pattern-1-2-1hd7)
- [Stop using isLoading booleans](https://kentcdodds.com/blog/stop-using-isloading-booleans/)
- [Guidelines for state machines and XState](https://kyleshevlin.com/guidelines-for-state-machines-and-xstate)
- [useState vs useReducer vs XState](https://dev.to/mpocock1/usestate-vs-usereducer-vs-xstate-part-1-modals-569e)

## Other (situational) good state managers

- [zustand](https://github.com/pmndrs/zustand)
- [recoil](https://recoiljs.org/)
- [immer](https://immerjs.github.io/immer/docs/introduction)

# Recommended starting stack

In addition to react, react-dom and the other starting modules you get with **create-react-app**, consider using the standard router for your SPA, and Material-UI as the UI component library.

- [React router](https://reactrouter.com/web/guides/quick-start)
- [Materail-UI](https://material-ui.com/)
  - Read through the docs on theming, to understand why and how you don't need any CSS file at all in your project.
- [clsx](https://www.npmjs.com/package/clsx)
- [ramda](https://ramdajs.com/)

`npm i @material-ui/core @material-ui/icons react-router-dom clsx`

Great standard utilities regarding data structures and date/time handling:

`npm i ramda date-fns`

# Circumstantial lib suggestions

For the different non-React libs below, great **React integration** libs probably exist. Just search npmjs.org for "<lib> react".

### Forms

[react-hook-form](https://react-hook-form.com/)
If you need a form or user input of any kind, this might be the best option for react. It has great performance.

### Tables

[material-table](https://material-table.com/)
This is the most popular high abstraction table libs. It uses @material-ui internally and has many built in features.
**NOTE:** This lib seems to have frozen in accepting bug fixes, and it has some major memory-leak issue. There is a well working community fork at `@material-table/core`.

Other (more low level) table libs worth checking out:

- [react-data-table-component](https://www.npmjs.com/package/react-data-table-component)
- [react-table](https://react-table.tanstack.com/)

### Multi-language

[i18next](https://www.i18next.com/)
If you need your app in multiple languages, this is the to-go solution.

Another good solution is [messageformat](https://www.npmjs.com/package/messageformat)

### Graphs/Charts

It is rather common with licenced data visualization libs, but here are some nice, free & open source ones:

#### [Chart.js](https://www.chartjs.org/)

Popular, lightweight & performant lib with basic graphs, nice API, configurability and plugins.

- [npm](https://www.npmjs.com/package/chart.js) downloads: 1400k
- [github](https://github.com/chartjs/Chart.js) stars: 53k
- [React wrapper](https://www.npmjs.com/package/react-chartjs-2)
  - npm downloads: 300k

#### [ECharts](https://echarts.apache.org/)

If you need a **much** more feature rich and flashy chart lib. It is really well designed, with a very modular strategy which makes it powerful and easy to configure once you get the hang of it.

- [github](https://github.com/apache/echarts) stars: 46k
- [npm](https://www.npmjs.com/package/echarts) downloads: 300k
- [React wrapper](https://www.npmjs.com/package/echarts-for-react)
  - npm downloads: 60k

#### [Recharts](https://recharts.org/en-US)

React focused chart lib, built on D3.

- [npm](https://www.npmjs.com/package/recharts) downloads: 500k
- [github](https://github.com/recharts/recharts) stars: 16k

#### [Billboard.js](https://naver.github.io/billboard.js/)

Maintained fork of C3. Builds on D3.

- [github](https://github.com/naver/billboard.js) stars: 4.8k
- [npm](https://www.npmjs.com/package/billboard.js) downloads: 8k
- [React wrapper](https://www.npmjs.com/package/react-billboardjs)
  - npm downloads: 700

#### [Toast UI Chart](https://ui.toast.com/tui-chart)

No dependencies, responsive charts, with react bindings.

- v4
  - [github](https://github.com/nhn/tui.chart) stars: 4.9k
  - [npm](https://www.npmjs.com/package/@toast-ui/chart) downloads: 15k
- v3
  - [npm](https://www.npmjs.com/package/tui-chart) downloads: 15k
- [React wrapper](https://www.npmjs.com/package/@toast-ui/react-chart)
  - npm downloads: 1k

#### [dygraphs](https://dygraphs.com/)

If you need to display huge sets of data, or just want to try an alternative to Chart.js.

- [github](https://github.com/danvk/dygraphs) stars: 2.9k
- [npm](https://www.npmjs.com/package/dygraphs) downloads: 8k

### Maps

If you need free and open source maps in your app: [Leaflet](https://leafletjs.com/)

### Data utilities
  
[Joi](https://joi.dev/)
If you for instance work with APIs in NodeJS, or need validation of data structures elsewhere.

[useLocalStorageState](https://www.npmjs.com/package/use-local-storage-state)
Very simple & useful variant of the `useState` hook, that persists the state in local storage.

# Wrapping up

Build your SPA (Single Page Application) into a **lightweight container** by using a node image as a builder. In the Dockerfile (or possible other container script - podman?) use `npm ci` to install dependencies, **not** `npm i`/`npm install`. Then copy the bundle files into a minimal alpine webserver container (nginx).

## Maintenance of an npm repo

- **Regularly** check project dependency status: `npm outdated`.
  - If some items are red, they can safely be updated with: `npm update`.
  - Manually upgrade major/breaking new versions by reading changelog/docs and making appropriate code changes if needed.
  - For React apps: **Don't update `eslint`** manually, it will probably collide with `react-scripts`.
- `npm audit` / `npm audit fix` to solve vulnerability warnings.
- Always push the `package-lock.json` file if locally (automatically) changed.

- If something behaves strange/incorrect/unexpected regarding dependencies or npm:
  - First try `npm doctor` to get ideas of what could be wrong or problematic.
  - Remove node_modules/ (`rm -rf node_modules`).
  - Remove package-lock.json (`rm package-lock.json`).
  - Run `npm i`.
  - Then do what you tried before and see if it works better.


---

# Solution to "Intermediate React-JavaScript challenge" above

```js
// Basic solution
const ARR = []; // Use this as default value for items in App header

// Better, permanent solution for real world apps
// In some utils file:
export const ARR = Object.freeze([]);
/* eslint-disable no-null/no-null */
export const OBJ = Object.freeze(Object.create(null));
/* eslint-enable no-null/no-null */
export const FUNC = Function.prototype;

// (If you use functional rules in your eslint config, as suggested, you don't actually have to freeze them)
```
