# Typescript / strong typing

There are many different type checking systems out there for JS & React, and there are always pros, cons & trade-offs with any choice. There are both run-time checking alternatives, and static checking + transpilation alternatives. A common misunderstanding is that e.g. Typescript gives run-time type checking, which it can't.

Common static systems:

- Typescript
- Flow
- PropTypes (can also be used in runtime, especially in development)
- JSDoc
- Rtype

And some run-time options:

- Joi
- Sanctuary

There is a rather vivid discussion out there about TypeScript and its pros & cons over native JS (ES2019+). I would like to share a few resources on the matter that could be worth checking out:

- https://medium.com/javascript-scene/the-shocking-secret-about-static-types-514d39bf30a3
- https://medium.com/javascript-scene/the-typescript-tax-132ff4cb175b
- Read also "Type Inference" under "Default parameters" in
  - https://medium.com/javascript-scene/javascript-factory-functions-with-es6-4d224591a8b1#5152

So, a common misunderstanding is that TypeScript is great at preventing bugs, but its stronger card has to do with development tooling. Instead, functional style programming seems to help in preventing bugs, and then of course testing & TDD.

Another misunderstanding is that TypeScript helps with checking types everywhere and always in the application, but no dynamic (run-time) data will be checked, and especially frontends often deal with dynamic data -- coming from user input or from various fetches externally.

Another comment that I feel is worth mentioning, is that, ideally "The best function/component is one that accepts and works for all existing types". This is of course not at all practical in most cases, but strong typing everywhere pushes development in the other direction, and in cases where functions and components can come close to this ideal, strict typing does not help in preventing bugs but only adds additional work/cost.

A small example of a component that is highly dynamic, accepts almost all types, and that is very common in React FE solutions is clsx: https://www.npmjs.com/package/clsx

## Summary
A good choice is then to use clean JS, with PropTypes (as airbnb eslint config enforces), and utilize linting a lot as primary checking/help for developers. Also to have testing as the main strategy for bug prevention, but also push a bit toward functional style concepts & patterns like:

- immutability
- pure functions
- declarative code over imperative
- composition over inheritance (avoid classes altogether)
- separation of data & logic
- etc.

# ReScript

A very interesting new choice would be to try [ReScript](https://rescript-lang.org/). Some info from the website:

> ReScript is a robustly typed language that compiles to efficient and human-readable JavaScript. It comes with a lightning fast compiler toolchain that scales to any codebase size.

> Leverage the full power of JavaScript in a robustly typed language without the fear of `any` types.

> ReScript is used to ship and maintain mission-critical products with good UI and UX.

> ReScript offers first class bindings for ReactJS and are designed and built by people using ReScript and React in large mission critical React codebases. The bindings are compatible with modern React versions (>= v16.8).
