import * as R from "ramda";

const step = (steps = 18, players = 16) =>
  steps <= 0
    ? Math.max(0, players)
    : step(steps - 1, Math.random() > 0.5 ? players : players - 1);

// const survivors = step();

const average = R.converge(R.divide, [R.sum, R.length]);

const survivors = R.pipe(
  R.map(() => step()),
  R.tap(console.log),
  average
)(R.range(1, 100_000));

console.log(survivors);
