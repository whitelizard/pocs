// ----- UTILS -----

const wait = (ms) => new Promise((r) => setTimeout(r, ms));

const triggerPromise = () => {
  let resolve;
  const promise = new Promise((r) => (resolve = r));
  return [promise, resolve];
};

const asyncWithTimeout = (promise, timeout = 900) =>
  Promise.race([
    promise,
    new Promise((_, reject) => setTimeout(() => reject(Error('Async timeout')), timeout)),
  ]);

// ----- MOCKS -----

const subs = {};
const subscribe = (ch, cb) => (subs[ch] = cb);
const publish = (ch, msg) => {
  if (subs[ch]) subs[ch](msg);
};

// ----- TESTS -----

describe('publish/subscribe 1', () => {
  it('should send a message from publish to subscribe', async () => {
    expect.assertions(1);
    let result;
    subscribe('ch1', (msg) => (result = msg));
    publish('ch1', 1);
    await wait(900);
    expect(result).toBe(1);
  });
});

describe('publish/subscribe 2', () => {
  it('should send a message from publish to subscribe', async () => {
    expect.assertions(1);
    let result;
    const [promise, resolve] = triggerPromise();
    subscribe('ch1', (msg) => {
      result = msg;
      resolve();
    });
    publish('ch1', 1);
    await promise;
    expect(result).toBe(1);
  });
});

describe('publish/subscribe 3', () => {
  it('should send a message from publish to subscribe', async () => {
    expect.assertions(1);
    let result;
    const [promise, resolve] = triggerPromise();
    subscribe('ch1', (msg) => {
      result = msg;
      resolve();
    });
    publish('ch1', 1);
    await asyncWithTimeout(promise);
    expect(result).toBe(1);
  });
});

// ----- READER SUGGESTION -----

test('publish-subscribe', async (t) => {
  const promise = new Promise((resolve, reject) => {
    subscribe('channel1', (msg) => resolve(msg));

    setTimeout(() => reject(Error('Async timeout')), 3000);
  });

  publish('channel1', 1);

  const result = await promise;

  t.equals(result, 1);
});
