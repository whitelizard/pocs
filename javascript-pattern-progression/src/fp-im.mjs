export const deepFreeze = item => {
  const complex = Object(item) === item;
  if (Object.isFrozen(item) || !complex) return item;
  Object.values(item).forEach(deepFreeze);
  return Object.freeze(item);
};

export const createStore = (initState = {}, reducer) => {
  let state = deepFreeze(initState);
  return {
    getState: key => (key ? state[key] : state),
    dispatch: action => (state = deepFreeze(reducer(state, action))),
  };
};
