import { incHits, area, intersect, setPos } from './fp.mjs';

export const makeCircle = (pos = [0, 0], r = 0) => {
  let state = { r, pos, hits: 0 };
  return {
    getState: () => state,
    incHits: () => (state = incHits(state)),
    setPos: pos => (state = setPos(state, pos)),
    area: () => area(r),
    intersect: c2 => {
      let hit;
      [hit, state] = intersect(state, c2.getState());
      if (hit) c2.incHits();
      return hit;
    },
  };
};
