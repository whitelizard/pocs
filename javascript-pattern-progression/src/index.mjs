import "./styles.css";

import "./class/class.test";
import "./constructor/constructor.test";
import "./factory/factory.test";
import "./fp/fp.test";
import "./fp-factory/fp-factory.test";
import "./fp-im/fp-im.test";
