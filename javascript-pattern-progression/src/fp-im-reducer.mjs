import { setPos, incHits } from './fp.mjs';

export const reducer = (state, action) => {
  const actions = {
    move: ({ id, payload }) => ({ ...state, [id]: setPos(state[id], payload) }),
    hit: ({ id1, id2 }) => ({
      ...state,
      [id1]: incHits(state[id1]),
      [id2]: incHits(state[id2]),
    }),
  };
  return actions[action.type](action);
};
