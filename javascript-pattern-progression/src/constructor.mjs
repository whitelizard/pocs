export function Circle(pos = [0, 0], r = 0) {
  this.pos = pos;
  let hits = 0;

  this.getR = () => r;
  this.getHits = () => hits;
  this.incHits = () => (hits += 1);
  this.area = () => r ** 2 * Math.PI;

  this.intersect = circle => {
    const h = Math.sqrt(
      (this.pos[0] - circle.pos[0]) ** 2 + (this.pos[1] - circle.pos[1]) ** 2,
    );
    const hit = h - r - circle.getR() < 0;
    if (hit) {
      this.incHits();
      circle.incHits();
    }
    return hit;
  };
}
