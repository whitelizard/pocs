export function makeCircle(pos = [0, 0], r = 0) {
  let hits = 0;

  const obj = {
    pos,
    getR: () => r,
    getHits: () => hits,
    incHits: () => (hits += 1),
    area: () => r ** 2 * Math.PI,
    intersect: (circle) => {
      const h = Math.sqrt(
        (obj.pos[0] - circle.pos[0]) ** 2 + (obj.pos[1] - circle.pos[1]) ** 2,
      );
      const hit = h - r - circle.getR() < 0;
      if (hit) {
        obj.incHits();
        circle.incHits();
      }
      return hit;
    },
  };
  return obj;
}
