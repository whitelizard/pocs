export class Circle {
  constructor(pos = [0, 0], r = 0) {
    this.pos = pos;
    this.radius = r;
    this.hitCount = 0;
  }

  get r() {
    return this.radius;
  }
  set r(_) {
    throw Error('Setting r is forbidden');
  }

  get hits() {
    return this.hitCount;
  }
  set hits(_) {
    throw Error('Can only increase hits, not set it');
  }

  incHits() {
    this.hitCount += 1;
  }

  area() {
    return this.r ** 2 * Math.PI;
  }

  intersect(circle) {
    const h = Math.sqrt((this.pos[0] - circle.pos[0]) ** 2 + (this.pos[1] - circle.pos[1]) ** 2);
    const hit = h - this.r - circle.r < 0;
    if (hit) {
      this.incHits();
      circle.incHits();
    }
    return hit;
  }
}
