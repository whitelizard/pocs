// Circle data object creator
export const circle = (pos = [0, 0], r = 0) => ({ pos, r, hits: 0 });

// Immutable-updates helpers
export const incHits = obj => ({ ...obj, hits: (obj.hits || 0) + 1 });
export const setPos = (obj, pos = [0, 0]) => ({ ...obj, pos });

// Circle computations
export const area = r => r ** 2 * Math.PI;

export const circlesIntersect = ({ r: r1, pos: [x1, y1] }, { r: r2, pos: [x2, y2] }) =>
  Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2) - r1 - r2 < 0;

export const intersect = (c1, c2) => {
  const hit = circlesIntersect(c1, c2);
  return hit ? [hit, incHits(c1), incHits(c2)] : [hit, c1, c2];
};
