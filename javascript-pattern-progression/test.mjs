import test from 'blue-tape';

const runTests = async () => {
  test('Class', async t => {
    const { Circle } = await import('./src/class.mjs');

    const c1 = new Circle([2.5, 0], 1.5);
    const c2 = new Circle([-2, 0], 2);

    t.equals(c1.area(), 7.0685834705770345);
    t.equals(c2.area(), 12.566370614359172);
    t.equals(c1.intersect(c2), false);
    t.equals(c1.hits, 0);

    c1.pos[0] = 1;
    t.equals(c1.intersect(c2), true);
    t.equals(c1.hits, 1);
    t.equals(c2.hits, 1);

    t.throws(() => (c1.hits = 4));

    t.equals(Circle.prototype.area.bind({ r: 4 })(), 50.26548245743669);
  });

  test('Constructor', async t => {
    const { Circle } = await import('./src/constructor.mjs');

    const c1 = new Circle([2.5, 0], 1.5);
    const c2 = new Circle([-2, 0], 2);

    t.equals(c1.area(), 7.0685834705770345);
    t.equals(c2.area(), 12.566370614359172);
    t.equals(c1.intersect(c2), false);
    t.equals(c1.getHits(), 0);

    c1.pos[0] = 1;
    t.equals(c1.intersect(c2), true);
    t.equals(c1.getHits(), 1);
    t.equals(c2.getHits(), 1);

    c1.hits = 4;
    t.equals(c1.getHits(), 1);
  });

  test('Factory', async t => {
    const { makeCircle } = await import('./src/factory.mjs');

    const c1 = makeCircle([2.5, 0], 1.5);
    const c2 = makeCircle([-2, 0], 2);

    t.equals(c1.area(), 7.0685834705770345);
    t.equals(c2.area(), 12.566370614359172);
    t.equals(c1.intersect(c2), false);
    t.equals(c1.getHits(), 0);

    c1.pos[0] = 1;
    t.equals(c1.intersect(c2), true);
    t.equals(c1.getHits(), 1);
    t.equals(c2.getHits(), 1);

    c1.hits = 4;
    t.equals(c1.getHits(), 1);
  });

  test('Simplistic FP', async t => {
    const { area, intersect, incHits, setPos, circle } = await import(
      './src/fp.mjs'
    );

    let c1 = circle([2.5, 0], 1.5);
    let c2 = circle([-2, 0], 2);
    const c2ref = c2; // Let's explore what immutability of c1/c2 means!

    t.equals(area(c1.r), 7.0685834705770345);
    t.equals(area(c2.r), 12.566370614359172);
    let hit;
    [hit, c1, c2] = intersect(c1, c2);
    t.equals(hit, false);
    t.equals(c1.hits, 0);

    c1 = setPos(c1, [1, 0]);
    [hit, c1, c2] = intersect(c1, c2);
    t.equals(hit, true);
    t.equals(c1.hits, 1);
    t.equals(c2.hits, 1);

    t.equals(c2ref.hits, 0);

    c1.hits = 4;
    t.equals(c1.hits, 4); // FAIL. Not what we want
  });

  test('FP-Factory', async t => {
    const { makeCircle } = await import('./src/fp-factory.mjs');

    const c1 = makeCircle([2.5, 0], 1.5);
    const c2 = makeCircle([-2, 0], 2);

    t.equals(c1.area(), 7.0685834705770345);
    t.equals(c2.area(), 12.566370614359172);
    t.equals(c1.intersect(c2), false);
    t.equals(c1.getState().hits, 0);

    c1.setPos([1, 0]);
    t.equals(c1.intersect(c2), true);
    t.equals(c1.getState().hits, 1);
    t.equals(c2.getState().hits, 1);

    c1.hits = 4;
    t.equals(c1.getState().hits, 1);

    // c1.getState().hits = 4;
    // t.equals(c1.getState("hits"), 1); // FAILS
  });

  test('FP + Immutability', async t => {
    const { createStore, deepFreeze } = await import('./src/fp-im.mjs');
    const { circle, intersect, area, setPos } = await import('./src/fp.mjs');
    const { reducer } = await import('./src/fp-im-reducer.mjs');

    const frz = deepFreeze;

    let c1 = frz(circle([2.5, 0], 1.5));
    let c2 = frz(circle([-2, 0], 2));

    t.equals(area(c1.r), 7.0685834705770345);
    t.equals(area(c2.r), 12.566370614359172);
    let hit;
    [hit, c1, c2] = frz(intersect(c1, c2));
    t.equals(hit, false);
    t.equals(c1.hits, 0);

    c1 = frz(setPos(c1, [1, 0]));
    [hit, c1, c2] = frz(intersect(c1, c2));
    t.equals(hit, true);
    t.equals(c1.hits, 1);
    t.equals(c2.hits, 1);

    t.throws(() => (c1.hits = 4));
    t.equals(c1.hits, 1);
  });

  test('FP + Immutability Reduxish', async t => {
    const { createStore } = await import('./src/fp-im.mjs');
    const { circle, intersect, area } = await import('./src/fp.mjs');
    const { reducer } = await import('./src/fp-im-reducer.mjs');

    const initialState = {
      1: circle([2.5, 0], 1.5),
      2: circle([-2, 0], 2),
    };
    const { getState, dispatch } = createStore(initialState, reducer);

    t.equals(area(getState(1).r), 7.0685834705770345);
    t.equals(area(getState(2).r), 12.566370614359172);
    const [hit] = intersect(getState(1), getState(2));
    t.equals(hit, false);
    if (hit) dispatch({ type: 'hit', id1: 1, id2: 2 });
    t.equals(getState(1).hits, 0);

    dispatch({ type: 'move', payload: [1, 0], id: 1 });
    const [hit2] = intersect(getState(1), getState(2));
    t.equals(hit2, true);
    if (hit2) dispatch({ type: 'hit', id1: 1, id2: 2 });
    t.equals(getState(1).hits, 1);
    t.equals(getState(2).hits, 1);

    t.throws(() => (initialState[1].hits = 4));
    t.throws(() => (getState(1).hits = 4));
    t.equals(getState(1).hits, 1);
  });
};

runTests();
